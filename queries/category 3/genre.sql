-- name: 'Genres'
-- updatemin: 5
-- sumfield: 'Genre'
-- sumtype: 'bars'
-- sumsort: 'descending'
-- percent: True

SELECT G.Name AS Genre, T.Name AS Track
FROM Track AS T
JOIN Genre AS G ON T.GenreId = G.GenreId
ORDER BY Genre, Track
