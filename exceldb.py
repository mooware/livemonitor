def load_xls(filename):
  """Load an excel file into an in-memory sqlite DB."""
  import xlrd

  book = xlrd.open_workbook(filename, formatting_info=True)
  sheet = book.sheet_by_index(0)

  # first row defines column names
  header = sheet.row_values(0)
  # collect all rows
  rows = [sheet.row(i) for i in range(1, sheet.nrows)]

  # column types for CREATE statement
  coltypes = _collect_sqlite_coltypes(rows, sheet.ncols)
  # convert from excel types to python types
  sqlrows = [ [_convert_cell(c, book) for c in row] for row in rows]

  return _create_sqlite_table(header, coltypes, sqlrows)


def run_query(filename, sql):
  """Run an SQL query against an sqlite DB built from an excel file."""
  conn = None
  cur = None
  try:
    conn = load_xls(filename)

    cur = conn.cursor()
    cur.execute(sql)

    result = cur.fetchall()
    columns = [row[0] for row in cur.description]

    return (result, columns)
  finally:
    if cur is not None:
      cur.close()
    if conn is not None:
      conn.close()


def _convert_cell(cell, book):
  """Apply some special conversions to excel cell values."""
  import xlrd

  if cell.ctype in (xlrd.XL_CELL_BLANK, xlrd.XL_CELL_EMPTY):
    return None
  elif cell.ctype == xlrd.XL_CELL_DATE:
    return xlrd.xldate.xldate_as_datetime(cell.value, book.datemode)
  elif cell.ctype == xlrd.XL_CELL_NUMBER and cell.value.is_integer:
    return int(cell.value)
  else:
    return cell.value


def _create_sqlite_table(colnames, coltypes, rows):
  """Create and fill an in-memory sqlite database."""
  import sqlite3

  conn = sqlite3.connect(':memory:')
  cur = conn.cursor()

  coldefs = []
  for (name, sqltype) in zip(colnames, coltypes):
    sqlname = name.replace('"', '')
    col = '"{}" {}'.format(sqlname, sqltype)
    coldefs.append(col)

  create_sql = 'CREATE TABLE excel({})'.format(', '.join(coldefs))
  cur.execute(create_sql)

  insert_sql = 'INSERT INTO excel VALUES({})'.format(','.join(['?'] * len(colnames)))
  cur.executemany(insert_sql, rows)

  return conn


def _collect_sqlite_coltypes(rows, ncols):
  """Determine appropriate sqlite column types for excel cells."""
  # if the table is empty, just use text for all columns
  import xlrd

  if not rows:
    return ['TEXT'] * ncols

  coltypes = []
  for colidx in range(ncols):
    coltype = None

    for rowidx in range(len(rows)):
      r = rows[rowidx][colidx]
      rowtype = None

      if r.ctype in (xlrd.XL_CELL_BLANK, xlrd.XL_CELL_EMPTY):
        pass # ignore
      elif r.ctype in (xlrd.XL_CELL_TEXT, xlrd.XL_CELL_ERROR):
        rowtype = 'TEXT'
      elif r.ctype == xlrd.XL_CELL_DATE:
        rowtype = 'DATE' # not a real sqlite type, handle later
      elif r.ctype == xlrd.XL_CELL_BOOLEAN:
        rowtype = 'INTEGER'
      elif r.ctype == xlrd.XL_CELL_NUMBER:
        # excel only has floats, check if they are integers
        rowtype = ('INTEGER' if r.value.is_integer else 'REAL')

      # make sure that types in different rows are consistent
      if rowtype:
        if coltype and rowtype != coltype:
          if coltype == 'INTEGER' and rowtype == 'REAL':
            coltype = 'REAL' # promote to float
          elif coltype == 'REAL' and rowtype == 'INTEGER':
            pass # keep it float
          else:
            raise Exception("inconsistent data types in excel file, col index {}".format(colidx))
        else:
          coltype = rowtype

    coltypes.append(coltype if coltype else 'TEXT')

  return coltypes
