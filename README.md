# LiveMonitor

LiveMonitor ist eine Web-Anwendung zur Anzeige von Datenbank-Abfragen. Für 
jede konfigurierte Abfrage gibt es eine separate Seite, welche die Ergebnisse 
anzeigt und als CSV-Download anbietet. Die Ergebnisse einer Abfrage werden 
zwischengespeichert und nur nach einer konfigurierbaren Zeitspanne neu 
abgefragt.

## Konfiguration

Konfiguration in config.py, naehere Details sind dort dokumentiert.

Abfragen als ".sql" im Ordner "queries" ablegen. Optionen werden am Anfang der
SQL-Datei angegeben, pro Option eine Zeile im Format "-- option: wert",
z.B. "-- name: 'Backlog'". Nach den Optionen folgt eine Leerzeile, danach die
Abfrage.

Folgende Optionen existieren:

* **name**: query name as displayed
* **updatemin**: optional, minimum minutes between updates of the query.
* **sumfield**: optional, field in the query on which the sums will be computed.
* **sumtype**: optional, determines how sums will be displayed. 'bars' for bar charts or 'table' for table.
* **sumsort**: optional, sorts the sum results by sum value, 'ascending' or 'descending'. by default, the sum results will have the same order as the query results.
* **percent**: optional, percentages will be printed for sums if True.
* **barcolors**: optional, list of colors for bar charts, in css color format.
* **excelfile**: optional, use excel file at given path as data source.

## Abhaengigkeiten:

* [Python 2.7](http://www.python.org/ftp/python/2.7.6/python-2.7.6.msi)
* [CherryPy](http://download.cherrypy.org/cherrypy/3.2.2/CherryPy-3.2.2.win32.exe)
* [pymssql](http://pymssql.googlecode.com/files/pymssql-2.0.0b1-dev-20130111.win32-py2.7.exe)
* [pywin32](http://sourceforge.net/projects/pywin32/files/pywin32/Build%20218/pywin32-218.win32-py2.7.exe/download)
* [xlrd](https://github.com/python-excel/xlrd)

## Ausführung

Ausführung als normales Programm:
```
$> python bottle.py --bind=0.0.0.0 livemonitor
```

Installation als Windows Service (Administrator-Rechte notwendig):
```
$> python winsvc.py --startup auto install
$> python winsvc.py start
```
