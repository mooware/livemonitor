from bottle import *
import config, cache, util
import os.path

# when running as service, have to set template path
BASE_PATH = os.path.dirname(os.path.abspath(__file__))
TEMPLATE_PATH.insert(0, os.path.join(BASE_PATH, 'views'))

def _get_query(folder, query):
  fdict = util.get_queries(folder)
  if not fdict:
    abort(404, 'Unknown folder')

  qdict = None
  for q in fdict['queries']:
    if q['url'] == query:
      qdict = q
  if not qdict:
    abort(404, 'Unknown query')

  return (fdict, qdict)

def _get_query_data(folder, query):
  update = bool(request.query.get('update', False))
  return cache.get_query_data((folder, query), update)

# hack for static file hosting,
# if no real webserver takes care of it
@route('/css/<filename>')
@route('/js/<filename>')
@route('/fonts/<filename>')
def file(filename):
  return static_file(request.path, root=BASE_PATH)

@route('/')
@route('/home')
def home():
  util.update_queries()
  folders = util.get_queries()
  return template('list', folders=folders)

@route('/list/<folder>')
def list_folder(folder):
  util.update_queries()
  f = util.get_queries(folder)
  if not f:
    abort(404, 'Unknown folder')
  return template('list', folder=f, folders=[f])

@route('/query/<folder>/<query>')
def query(folder, query):
  util.update_queries()
  (f, q) = _get_query(folder, query)
  (data, status) = _get_query_data(folder, query)
  return template('query', folder=f, query=q, data=data, status=status)

@route('/download/<folder>/<query>')
def download_query(folder, query):
  (data, status) = _get_query_data(folder, query)
  if not data or 'rows' not in data:
    abort(404, 'No result yet')

  filename = "{:}-{:%Y%m%d-%H%M%S}.csv".format(query, data['start'])
  response.set_header('Content-Disposition', 'attachment; filename=' + filename)
  response.set_header('Content-Type', 'text/csv')

  return util.to_csv(data['rows'], data['header'])

if __name__ == '__main__':
  run(host=config.HTTPHOST, port=config.HTTPPORT, server='cherrypy', quiet=True)
