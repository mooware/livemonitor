import cherrypy
import bottle
import win32serviceutil
import win32service
import config
import livemonitor

class LiveMonitorService(win32serviceutil.ServiceFramework):
  _svc_name_ = "LiveMonitor"
  _svc_display_name_ = "LiveMonitor Service"

  def SvcDoRun(self):
    cherrypy.config.update({
      'global': {
        'log.screen': False,
        'engine.autoreload.on': False,
        'engine.SIGHUP': None,
        'engine.SIGTERM': None
      }
    })

    #cherrypy.engine.start()
    #cherrypy.engine.block()
    livemonitor.run(server='cherrypy', host=config.HTTPHOST, port=config.HTTPPORT)

  def SvcStop(self):
    self.ReportServiceStatus(win32service.SERVICE_STOP_PENDING)
    cherrypy.engine.exit()

    self.ReportServiceStatus(win32service.SERVICE_STOPPED) 
    # very important for use with py2exe
    # otherwise the Service Controller never knows that it is stopped !

if __name__ == '__main__':
  win32serviceutil.HandleCommandLine(LiveMonitorService)
