import config
import bottle
import os, os.path, sys, re, glob

_BASE_PATH = os.path.dirname(os.path.abspath(__file__))
_QUERY_PATH = os.path.join(_BASE_PATH, "queries")

# modification time of queries folder
_QUERIES_MTIME = None

# query configuration, loaded from queries folder
_FOLDERS = []

# valid characters in an url
_VALID_URL_CHARS = re.compile('\w+') # alphanumeric

# format for sql config lines, e.g. "-- name: value"
_SQL_CONFIG_LINE = re.compile(r'^--\s+(\w+)\s*:\s*(.+)\s*$')

# list of valid config entries
_CONFIG_ENTRIES = [
  'name', 'url', 'updatemin', 'sql',
  'sumfield', 'sumtype', 'sumsort',
  'percent', 'barcolors', 'excelfile'
]

def _mtime(path):
  return os.stat(path).st_mtime

def debug(*args):
  """Print a debug message if the bottle debug option is enabled."""
  if bottle.DEBUG:
    args = map(unicode, args)
    if len(args) > 1:
      args[0] = '[{:}]'.format(args[0])
    msg = ' '.join(args)
    sys.stderr.write(msg + os.linesep)

def get_queries(folder=None):
  """Return the query configuration, loaded from 'queries/<folder>/<query>.sql'."""
  if not _FOLDERS:
    update_queries()
  if folder:
    for f in _FOLDERS:
      if f['url'] == folder:
        return f
  else:
    return _FOLDERS

def get_query(tuple_or_folder, url=None):
  """Return the query with the given url."""
  if url:
    folder = tuple_or_folder
  else:
    folder, url = tuple_or_folder
  if not folder:
    return
  f = get_queries(folder)
  if not f:
    return
  for q in f['queries']:
    if q.get('url') == url:
      return q

def invalid_config_entries(q):
  """Returns a list of invalid config entries in a query config dict."""
  return [k for k in q.keys() if k not in _CONFIG_ENTRIES]

def read_sql_file(filename):
  """Reads an SQL text file, either in UTF-16 or UTF-8. Returns a list of lines."""
  with open(filename, "rb") as f:
    encoded = f.read()
  s = None
  # try different encodings
  for encoding in ["utf-16", "utf-16-le", "utf-8", "latin1"]:
    try:
      s = encoded.decode(encoding)
      # must start with "-- "
      if s[0:3] == "-- ":
        break
      else:
        s = None
    except:
      s = None
  if s is None:
    raise Exception("file '{:}' has unknown encoding".format(filename))
  # split by "\n" or "\r\n"
  lines = s.replace("\r", "").split("\n")
  return lines

def read_query(filename):
  """Reads a query configuration file with the following format:
     First a number of configuration lines like '-- <config name>: <config value>',
     then an empty line, and in the rest of the file an SQL statement.
     Returns a query configuration dict."""
  cfg = {}
  mtime = _mtime(filename)
  lines = read_sql_file(filename)
  linenum = 0
  while lines:
    line = lines.pop(0)
    linenum += 1
    # empty line denotes start of sql statement
    if line.strip() == "":
      break
    match = _SQL_CONFIG_LINE.match(line)
    if not match:
      raise Exception("config file '{:}', line {:}: invalid format {:}"
                      .format(filename, linenum, repr(line)))
    (name, value) = match.groups()
    cfg[name] = eval(value)
  # rest is sql statement (pymssql doesn't handle unicode well)
  cfg['sql'] = os.linesep.join(lines).encode("utf-8")
  # generate url from filename
  if 'url' not in cfg:
    base = os.path.basename(filename)
    (url, ext) = os.path.splitext(base)
    cfg['url'] = url
  # must have name
  if 'name' not in cfg:
    raise Exception("config file '{:}', no name defined".format(filename))
  # invalid entries?
  invalid = invalid_config_entries(cfg)
  if invalid:
    raise Exception("config file '{:}', invalid entries: {:}"
                    .format(filename, ", ".join(invalid)))
  cfg['file'] = filename
  cfg['mtime'] = mtime
  return cfg

def read_folder(name, old_queries=None):
  fullpath = os.path.join(_QUERY_PATH, name)
  mtime = _mtime(fullpath)
  if old_queries:
    old_map = {q['file']: q for q in old_queries}
  else:
    old_map = {}
  files = glob.glob(os.path.join(fullpath, "*.sql"))
  new_queries = []
  for f in files:
    old_query = old_map.get(f)
    if old_query and _mtime(f) == old_query['mtime']:
      # did not change, keep old query
      new_queries.append(old_query)
      continue
    debug("reading query file '{:}'".format(f))
    try:
      q = read_query(f)
    except Exception as ex:
      debug("reading file '{:}' failed: {:}".format(f, ex))
      q = old_query
    new_queries.append(q)
  new_queries.sort(key=lambda q: q['name'])

  display_name = name
  try:
    with open(os.path.join(fullpath, "name.txt"), "rb") as f:
      s = f.read().decode("utf-8").strip()
      if len(s):
        display_name = s
  except:
    pass

  return {'url': name, 'name': display_name, 'mtime': mtime, 'queries': new_queries}

def update_queries():
  # check root mtime
  qmtime = _mtime(_QUERY_PATH)
  global _QUERIES_MTIME, _FOLDERS
  if qmtime == _QUERIES_MTIME:
    return
  _QUERIES_MTIME = qmtime
  # check mtime for all folders, update accordingly
  old_map = {f['url']: f for f in _FOLDERS}
  new_folders = []
  for f in os.listdir(_QUERY_PATH):
    if not f.startswith("."):
      fullpath = os.path.join(_QUERY_PATH, f)
      if os.path.isdir(fullpath):
        old_folder = old_map.get(f)
        if old_folder:
          if _mtime(fullpath) == old_folder['mtime']:
            # did not change, keep old queries
            new_folders.append(old_folder)
            continue
          old_queries = old_folder['queries']
        else:
          old_queries = []
        new_folder = read_folder(f, old_queries)
        if new_folder['queries']:
          new_folders.append(new_folder)
  new_folders.sort(key=lambda f: f['url'])
  _FOLDERS = new_folders

def sum_rows(rows, sumindex, sortorder=None):
  sumdict = {}
  for r in rows:
    value = unicode(r[sumindex])
    count = sumdict.get(value, 0)
    sumdict[value] = count + 1
  result = sumdict.items()
  if sortorder:
    result.sort(key=lambda r: r[1])
    if sortorder == 'ascending':
      pass # already sorted ascending
    elif sortorder == 'descending':
      result.reverse()
    else:
      raise ValueError('invalid sort order \'{:}\''.format(sortorder))
  return result

def csv_line(fields):
  """Converts a list of string-convertible values to a line of CSV."""
  escaped_fields = [u'"' + unicode(s).replace(u'"', u'""') + u'"' for s in fields]
  return u';'.join(escaped_fields) + u'\r\n'

def to_csv(lines, header=None):
  """Generator for converting lines of fields into unicode csv text."""
  if header is not None:
    yield csv_line(header)
  for line in lines:
    yield csv_line(line)

def make_url(*args):
  import urllib
  parts = [urllib.quote(s) for s in args]
  return u'/' + u'/'.join(parts)
