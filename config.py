# on which interface should the http server listen? 0.0.0.0 means all interfaces.
HTTPHOST = '0.0.0.0'
HTTPPORT = 80

# database credentials
SQLITE_DB = 'chinook.sqlite'

DEFAULT_BARCOLORS = [
    '#0074d9', # BLUE
    '#7fdbff', # AQUA
    '#2ecc40', # GREEN
    '#01ff70', # LIME
    '#ffdc00', # YELLOW
    '#ff851b', # ORANGE
    '#ff4136', # RED
    '#b10dc9', # PURPLE
    '#f012be'  # FUCHSIA
]
