from datetime import datetime, timedelta
import threading
import traceback
import db, exceldb, util

# enums for query state
UP_TO_DATE = 1
QUERY_RUNNING = 2
NEW_QUERY = 3

# wait this long for results when starting a new query
_NEW_QUERY_WAIT_SEC = 1.0

_lock = threading.Lock()
# dict for threads, one thread per query
_threads = {}
# dict for query results
_cache = {}


def _run_query(q):
  util.debug(q['name'], "starting query")
  start = datetime.now()

  if 'excelfile' in q:
    (rows, header) = exceldb.run_query(q['excelfile'], q['sql'])
  else:
    (rows, header) = db.run_query(q['sql'])

  end = datetime.now()
  util.debug(q['name'], "query done, rows={:}".format(len(rows)))

  sumfield = q.get('sumfield')
  if sumfield:
    sumindex = header.index(sumfield)
    sumsort = q.get('sumsort')
    sums = util.sum_rows(rows, sumindex, sumsort)
  else:
    sums = None

  return {
    'rows': rows,
    'sums': sums,
    'header': header,
    'start': start,
    'end': end,
    'mtime': q['mtime']
  }

def _query_thread(query):
  try:
    q = util.get_query(query)
    res = _run_query(q)
    with _lock:
      _cache[query] = res
      _threads.pop(query)
  except Exception as e:
    util.debug(query, 'exception', e)
    with _lock:
      _cache[query].update({
        'error': e,
        'errortime': datetime.now(),
        'errortrace': traceback.format_exc()
      })
      _threads.pop(query)

def get_query_data(query, forceupdate=False):
  if not util.get_query(query):
    return None
  with _lock:
    # at least return an empty dict
    if not _cache.has_key(query):
      _cache[query] = {}
    # do we have any data?
    data = _cache.get(query)
    if data and data.has_key('rows'):
      # is the data recent enough?
      q = util.get_query(query)
      updatemin = q.get('updatemin', 0)
      limit = timedelta(0, 60 * updatemin)
      age = datetime.now() - data['start']
      if age < limit and q['mtime'] == data['mtime'] and not forceupdate:
        return (data, UP_TO_DATE)
    # if the query thread is currently not running, start a new one
    thread = _threads.get(query)
    if thread:
      return (data, QUERY_RUNNING)
    else:
      thread = threading.Thread(target=_query_thread, args=(query,))
      _threads[query] = thread
      thread.start()
  # let's wait for a bit, maybe the query is done soon
  thread.join(_NEW_QUERY_WAIT_SEC)
  if not thread.is_alive():
    with _lock:
      data = _cache.get(query)
      return (data, UP_TO_DATE)
  else:
    return (data, NEW_QUERY)
