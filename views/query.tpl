% rebase('base.tpl')
% import cache, config, util
% from util import make_url as url
% DATEFMT = '%d.%m. %H:%M:%S'

<div class="alert alert-info">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <p>
% if data.has_key('start'):
%   duration = data['end'] - data['start']
%   dur_sec = round(duration.total_seconds(), 2)
    Letzte Abfrage von {{data['start'].strftime(DATEFMT)}},
    lief {{dur_sec}}s.
% else:
    Noch keine fertige Abfrage vorhanden.
% end
% if status == cache.NEW_QUERY:
    Neue Abfrage wurde gestartet.
% elif status == cache.QUERY_RUNNING:
    Neue Abfrage l&auml;uft.
% end
  </p>
</div>

% if data.has_key('error'):
<div class="alert alert-danger alert-dismissable">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
  <p>
    <a class="alert-link toggle-link" data-toggle="collapse" data-target="#alert-details">
      <span class="glyphicon glyphicon-plus-sign"></span>
      <strong>Fehler von {{data['errortime'].strftime(DATEFMT)}}:</strong>
    </a>
    {{str(data['error'])}}
  </p>
  <pre id="alert-details" class="collapse">{{data['errortrace']}}</pre>
</div>
% end

<h2 class="title">
  <a href="{{url('query', folder['url'], query['url'])}}" title="refresh" class="btn btn-default btn-lg">
    <span class="glyphicon glyphicon-refresh"></span>
  </a>
  {{query.get('name')}}
</h2>

% if not data.has_key('rows'):
<div class="well">Noch keine Daten.</div>
% end

% has_sums = data.has_key('sums') and query.get('sumtype')
% if has_sums:
<div id="sum-panel" class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">Summen</h2>
  </div>
  <div id="sum-details">
    <div class="panel-body">
  % sums = data['sums']
  % maxsum = 0
  % rowcount = len(data.get('rows', []))
  % for (field, sum) in sums:
    % maxsum = max(sum, maxsum)
  % end
  % if query.get('sumtype') == 'bars':
    % barcolors = query.get('barcolors', config.DEFAULT_BARCOLORS)
    % for (index, (field, sum)) in enumerate(sums):
      % total_percent = round((float(sum) / float(rowcount)) * 100, 2)
      % bar_percent = round((float(sum) / float(maxsum)) * 100, 2)

      <div>
        {{field}}: {{sum}}
      % if query.get('percent'):
        ({{total_percent}}%)
      % end
      </div>
      <div class="progress">
        <div class="progress-bar" role="progressbar"
             aria-valuenow="{{sum}}" aria-valuemin="0" aria-valuemax="{{maxsum}}"
             style="width: {{bar_percent}}%;
      % if barcolors:
             background-color: {{barcolors[index % len(barcolors)]}};
      % end
             ">
        </div>
      </div>
    % end
  % elif query.get('sumtype') == 'table':
      <table id="sum-table" class="table table-bordered table-condensed">
        <tbody>
    % for (field, sum) in sums:
          <tr>
            <td>{{field}}</td>
            <td>{{sum}}</td>
      % if query.get('percent'):
            <td>{{total_percent}}%</td>
      % end
          </tr>
    % end
        </tbody>
      </table>
  % end
    </div>
  </div>
</div>
% end

% if data.has_key('rows'):
% rows = data['rows']
<div id="details-panel" class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">
      <a class="toggle-link" title="expand/collapse" data-toggle="collapse" data-target="#details-body">
        <span class="glyphicon glyphicon-plus-sign"></span>
        Details ({{len(rows)}} Zeilen)
      </a>
      <a href="{{url('download', folder['url'], query['url'])}}" title="download" class="pull-right">
        <span class="glyphicon glyphicon-download-alt"></span>
      </a>
    </h2>
  </div>
  <div id="details-body" class="panel-collapse collapse {{'' if has_sums else 'in'}}">
    <div class="panel-body">
      <table id="details-table" class="table table-bordered table-condensed">
        <thead>
          <tr>
  % for field in data['header']:
            <th>{{field}}</th>
  % end
          </tr>
        </thead>
        <tbody>
  % for row in data['rows']:
          <tr>
    % for field in row:
            <td>{{field}}</td>
    % end
          </tr>
  % end
        </tbody>
      </table>
    </div>
  </div>
</div>
% end
