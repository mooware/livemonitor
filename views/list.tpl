% rebase('base.tpl')
% from util import make_url as url
<div id="link-panel" class="panel panel-default">
  <div class="panel-heading">
    <h2 class="panel-title">Abfragen</h2>
  </div>
  <div id="link-details">
    <div class="panel-body">
% for f in folders:
      <h3><a href="{{url('list', f['url'])}}">{{f['name']}}</a></h3>
%   for q in f['queries']:
      <a href="{{url('query', f['url'], q['url'])}}">{{q['name']}}</a>
%   end
% end
    </div>
  </div>
</div>
