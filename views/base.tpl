% from util import make_url as url
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    % if defined('query'):
    <title>Live Monitor - {{query.get('name')}}</title>
    % elif defined('folder'):
    <title>Live Monitor - {{folder['name']}}</title>
    % else:
    <title>Live Monitor</title>
    % end
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/livemonitor.css" rel="stylesheet">
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
  </head>

  <body>
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">Live Monitor</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
          % if defined('folder'):
          %   for q in folder['queries']:
          %     active = defined('query') and q['name'] == query['name']
            <li {{!'class="active"' if active else ''}}>
              <a href="{{url('query', folder['url'], q['url'])}}">{{q['name']}}</a>
            </li>
          %   end
          % end
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      {{!base}}
    </div><!-- /.container -->

  </body>
</html>
