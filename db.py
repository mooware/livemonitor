import config

# default seems to be 60, way too long
_CONNECT_TIMEOUT = 5
# use sqlite instead of mssql for testing
_SQLITE_DB = config.__dict__.get('SQLITE_DB')

def _connect():
    if _SQLITE_DB:
        import sqlite3
        return sqlite3.connect(database=_SQLITE_DB)
    else:
        import pymssql
        return pymssql.connect(server=config.DBSERVER,
                               database=config.DBDATABASE,
                               user=config.DBUSER,
                               password=config.DBPASSWORD,
                               login_timeout=_CONNECT_TIMEOUT,
                               charset='utf8')

def run_query(sql):
  conn = None
  cur = None
  try:
    conn = _connect()

    cur = conn.cursor()
    cur.execute(sql)

    result = cur.fetchall()
    columns = [row[0] for row in cur.description]

    return (result, columns)
  finally:
    if cur is not None:
      cur.close()
    if conn is not None:
      conn.close()
